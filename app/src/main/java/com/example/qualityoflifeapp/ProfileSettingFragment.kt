package com.example.qualityoflifeapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Switch
import androidx.fragment.app.Fragment
import com.example.qualityoflifeapp.profiles.Profile

class ProfileSettingFragment : Fragment() {

    lateinit var activeProfile: Profile
    lateinit var radioGroup: RadioGroup
    lateinit var switchWiFi: Switch
    lateinit var switchBluetooth: Switch
    lateinit var switchDND: Switch

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadProfile()
        val myView = inflater.inflate(R.layout.settings_activity, container, false)
        radioGroup = myView.findViewById(R.id.radioGroup) as RadioGroup
        switchWiFi = myView.findViewById(R.id.wi_fi_switch)
        switchBluetooth = myView.findViewById(R.id.bluetooth_switch)
        switchDND = myView.findViewById(R.id.dnd_switch)

        loadProfileIntoView(myView)

        switchWiFi.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                activeProfile.wifi = true
            } else if (!isChecked) {
                activeProfile.wifi = false
            }
        }

        switchBluetooth.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                activeProfile.bluetooth = true
            } else if (!isChecked) {
                activeProfile.bluetooth = false
            }
        }

        switchDND.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                activeProfile.dnd = true
            } else if (!isChecked) {
                activeProfile.dnd = false
            }
        }

        radioGroup.setOnCheckedChangeListener { _, i ->
            if (i == R.id.radioButton_loud) {
                activeProfile.soundProfile = listOf(true, false, false)
            } else if (i == R.id.radioButton_vibrate) {
                activeProfile.soundProfile = listOf(false, true, false)
            } else if (i == R.id.radioButton_silent) {
                activeProfile.soundProfile = listOf(false, false, true)
            }
        }
        return myView
    }

    /**
     * Loads the [Profile] 
     */
    private fun loadProfileIntoView(myView: View) {
        when {
            activeProfile.wifi -> {
                switchWiFi.isChecked = true
            }
            activeProfile.bluetooth -> {
                switchBluetooth.isChecked = true
            }
            activeProfile.dnd -> {
                switchDND.isChecked = true
            }
        }
        when {
            activeProfile.soundProfile[0] -> {
                myView.findViewById<RadioButton>(R.id.radioButton_loud).isChecked = true
            }
            activeProfile.soundProfile[1] -> {
                myView.findViewById<RadioButton>(R.id.radioButton_vibrate).isChecked = true
            }
            activeProfile.soundProfile[2] -> {
                myView.findViewById<RadioButton>(R.id.radioButton_silent).isChecked = true
            }
        }
    }

    private fun loadProfile() {
        val profileName: String? = arguments?.getString("profile_name")
        if (profileName.equals("Home")) {
            activeProfile = Profile.generateStandardHomeProfile()
        } else if (profileName.equals("Outdoor")) {
            activeProfile = Profile.generateStandardOutdoorProfile()
        } else if (profileName.equals("Work")) {
            activeProfile = Profile.generateStandardWorkProfile()
        }
    }
}