package com.example.qualityoflifeapp.profiles

/**
 * This class stores the information of the profiles that
 * a user is able to activate.
 */
data class Profile(
    val name: String
) {
    var wifi: Boolean = false
    var bluetooth: Boolean = false
    var soundProfile: List<Boolean> = listOf(true, false, false)
        /**
         * This List contains the info of the different sound profiles.
         * In order -> [Loud, Vibrate, Silent].
         */
        set(value) {
            var checkSum = 0
            for (i in value) {
                if (i) checkSum += 1
            }
            if (checkSum != 1) {
                throw IllegalArgumentException("There can only be one active sound-profile field")
            }
            field = value
        }
    var dnd: Boolean = false

    companion object {

        fun generateStandardHomeProfile(): Profile {
            val profile: Profile = Profile("Home")
            profile.soundProfile = listOf(false, true, false)
            profile.wifi = true
            profile.bluetooth = false
            profile.dnd = false
            return profile
        }

        fun generateStandardOutdoorProfile(): Profile {
            val profile: Profile = Profile("Outdoor")
            profile.soundProfile = listOf(true, false, false)
            profile.wifi = false
            profile.bluetooth = true
            profile.dnd = false
            return profile
        }
        fun generateStandardWorkProfile(): Profile {
            val profile: Profile = Profile("Work")
            profile.soundProfile = listOf(false, false, true)
            profile.wifi = true
            profile.bluetooth = false
            profile.dnd = false
            return profile
        }
    }
}