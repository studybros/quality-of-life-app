package com.example.qualityoflifeapp.profiles

import android.bluetooth.BluetoothManager
import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.qualityoflifeapp.R
import com.example.qualityoflifeapp.systemServices.SystemServices
import com.google.android.material.card.MaterialCardView


/**
 * [RecyclerView.Adapter] that can display a [Profile].
 */
class MyAdapter(private val myDataset: List<Profile>) :
    RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    inner class MyViewHolder(cardView: MaterialCardView) : RecyclerView.ViewHolder(cardView) {
        val profileTitle: TextView = cardView.findViewById(R.id.card_profile_title)
        val editButton: Button = cardView.findViewById(R.id.edit_button)
        val startButton: Button = cardView.findViewById(R.id.start_button)
        val wifiAdapter = cardView.context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val bluetoothManager =
            cardView.context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager

        override fun toString(): String {
            return super.toString() + " '" + profileTitle.text + "'"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.profile_cards, parent, false) as MaterialCardView
        return MyViewHolder(cardView)
    }

    override fun getItemCount() = myDataset.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val profile = myDataset[position]
        holder.profileTitle.text = profile.name
        holder.editButton.setOnClickListener {
            val bundle: Bundle = bundleOf()
            bundle.putString("profile_name", profile.name)
            holder.itemView.findNavController().navigate(R.id.action_Profiles_to_settingsFragment2, bundle)
        }
        holder.startButton.setOnClickListener {
            SystemServices.applyProfile(profile, holder.wifiAdapter, holder.bluetoothManager)
        }
    }
}