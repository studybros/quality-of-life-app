package com.example.qualityoflifeapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.qualityoflifeapp.profiles.MyAdapter
import com.example.qualityoflifeapp.profiles.Profile
import kotlinx.android.synthetic.main.list_profiles.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.list_profiles, container, false)
    }

    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(itemView, savedInstanceState)
        profile_list_recycle_view.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
            adapter = MyAdapter(listOf(
                Profile.generateStandardHomeProfile(),
                Profile.generateStandardOutdoorProfile(),
                Profile.generateStandardWorkProfile()
            ))
        }
    }
}