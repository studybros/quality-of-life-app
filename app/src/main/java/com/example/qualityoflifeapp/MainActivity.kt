package com.example.qualityoflifeapp

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.nfc.FormatException
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.nfc.Tag
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.qualityoflifeapp.nfc.NfcUtils
import com.example.qualityoflifeapp.nfc.StringUtils
import com.example.qualityoflifeapp.nfc.WritableTag

class MainActivity : AppCompatActivity() {
    private var adapter: NfcAdapter? = null
    var tag: WritableTag? = null
    var tagId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)
        //inits NFC adapter
        initNfcAdapter()
        //checks if the nfc adapter is enabled and notifies the user about the status
        val nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        if (nfcAdapter == null) {
            Toast.makeText(this, "noAdaperFound",Toast.LENGTH_LONG).show()
        } else if (!nfcAdapter.isEnabled) {
            Toast.makeText(this, "NFC available", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "NFC not available", Toast.LENGTH_LONG).show()
        }
    }

    private fun initNfcAdapter() {
        val nfcManager = getSystemService(Context.NFC_SERVICE) as NfcManager
        adapter = nfcManager.defaultAdapter
    }
// write_tag referenziert einen button aber aus irgendeinem grund ned via findviewbyid
//    private fun initViews() {
//        write_tag.setOnClickListener {
//            writeNDefMessage()
//        }
//    }

//    private fun writeNDefMessage() {
//        val message = NfcUtils.prepareMessageToWrite(StringUtils.randomString(44), this)
//        val writeResult = tag!!.writeData(tagId!!, message)
//        if (writeResult) {
//            showToast("Write successful!")
//        } else {
//            showToast("Write failed!")
//        }
//    }

    //Zeigt message auf einem toast an
    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        enableNfcForegroundDispatch()
    }

    override fun onPause() {
        disableNfcForegroundDispatch()
        super.onPause()
    }

    private fun disableNfcForegroundDispatch() {
        try {
            adapter?.disableForegroundDispatch(this)
        } catch (ex: java.lang.IllegalStateException) {
            Log.e(getTag(), "Error disabling NFC foreground dispatch", ex)
        }
    }

    private fun enableNfcForegroundDispatch() {
        try {
            val intent = Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            val nfcPendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            adapter?.enableForegroundDispatch(this, nfcPendingIntent, null, null)
        } catch (ex: IllegalStateException) {
            Log.e(getTag(), "Error enabling NFC foreground dispatch", ex)
        }
    }

    private fun getTag() = "MainActivity"

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val tagFromIntent = intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)
        try {
            tag = WritableTag(tagFromIntent)
        } catch (e: FormatException) {
            Log.e(getTag(), "Unsupported tag tapped", e)
            return
        }
        tagId = tag!!.tagId
        showToast("Tag tapped: $tagId")
    }

//    private fun onTagTapped(superTagId: String, superTagData: String){
//        tag_uid.text= superTagId
//        tag_data.text=superTagData
//    }
}