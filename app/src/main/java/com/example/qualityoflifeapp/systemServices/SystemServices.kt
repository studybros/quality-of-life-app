package com.example.qualityoflifeapp.systemServices

import android.bluetooth.BluetoothManager
import android.media.AudioManager
import android.net.wifi.WifiManager
import com.example.qualityoflifeapp.profiles.Profile

class SystemServices {
    companion object {
        fun applyProfile(profile: Profile, wifiManager: WifiManager, bluetoothManager: BluetoothManager) {
            if (profile.wifi) {
                manipulateWifi(wifiManager, true)
            } else {
                manipulateWifi(wifiManager, false)
            }
            if (profile.bluetooth) {
                manipulateBluetooth(bluetoothManager, true)
            }else{
                manipulateBluetooth(bluetoothManager, false)
            }
            if (profile.soundProfile[0]){
                setToLoud()
            }
            if (profile.soundProfile[1]){
                setToVibrate()
            }
            if (profile.soundProfile[2]){
                setToSilent()
            }
        }


        private fun manipulateWifi(wifiManager: WifiManager, active: Boolean) {
            if (active)
                if (!wifiManager.isWifiEnabled)
                    wifiManager.isWifiEnabled = true
                else if (!active)
                    if (wifiManager.isWifiEnabled)
                        wifiManager.isWifiEnabled = false
        }

        private fun manipulateBluetooth(bluetoothManager: BluetoothManager, active: Boolean) {
            if (active) {
                if (!bluetoothManager.adapter.isEnabled) {
                    bluetoothManager.adapter.enable()
                    /*
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(intent, REQUEST_ENABLE_BT)
                 */
                }
            } else if (!active)
                if (bluetoothManager.adapter.isEnabled)
                    bluetoothManager.adapter.disable()
        }

        private fun setToVibrate() {
            AudioManager.RINGER_MODE_VIBRATE
        }

        private fun setToLoud() {
            AudioManager.RINGER_MODE_NORMAL
        }

        private fun setToSilent() {
            AudioManager.RINGER_MODE_SILENT
        }
    }

}